const express = require("express");
const request = require('request');
const cheerio = require("cheerio");

var app = express();
var URL = "http://www.cbr.ru/currency_base/dynamics/?UniDbQuery.Posted=True&UniDbQuery.mode=1&UniDbQuery.date_req1=&UniDbQuery.date_req2=&UniDbQuery.VAL_NM_RQ=R01010&UniDbQuery.FromDate=24.03.1992&UniDbQuery.ToDate=08.03.2019";

app.listen(3000, function () {
	console.log("Example app listening on port 3000!");
})

app.get("/", (req, res) => {
	request(URL, function (error, response, body) {
	    if (!error) {
	        var $ = cheerio.load(body);
	        var dataJSON = $("table.data").text()

	        dataJSON = dataJSON.replace(/\s+/g, " ")
							   .split(" ");

	        var i = 6; //тк первые приходят заголовки
			var data = [];

	        while (i < dataJSON.length) {
	        	data.push({
	        		month: dataJSON[i],
	        		value: dataJSON[i+2]
	        	})

	        	i = i + 3;
	        }

	        res.json({
	        	status: true,
	        	data: data
	        })
	    } else {
	        console.log("Произошла ошибка: " + error);

	        res.json({
	        	status: false,
	        	message: error
	        })
	    }
	});

});



