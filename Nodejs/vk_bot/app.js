const express = require("express");
const bodyParser = require("body-parser");
const path = require('path')
const processing = require("./processing");

const { PORT, CONFIRMATION } = require('./config')

const session = require('express-session');

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.listen(PORT, function () {
	console.log(`VK Bot is listening on port ${PORT}!`);
})



app.get("/", (req, res) => {
	res.json({
		hello: "hello"
	})
});




app.post("/", (req, res) => {
	const { body } = req;
	
	// 	res.status(200).send('ok')

	switch (body.type) {
		case 'confirmation':
			res.end(CONFIRMATION);
			break;

		case 'message_new':
			processing(body.object)
			res.end('ok');
			break;

		default:
			res.end('ok');
			break;
	}
	
});