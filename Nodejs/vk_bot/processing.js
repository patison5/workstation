const send = require('./send')

module.exports = async ({ user_id: userId, body: text }) => {
	// var { user_id, text } = data;
	await send(userId, text)
}