#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <Magic.h>

class Components : public Magic {
    public:
        string title;
        Components();
        Components(string title);
        virtual ~Components();

    protected:

    private:
};

#endif // COMPONENTS_H
