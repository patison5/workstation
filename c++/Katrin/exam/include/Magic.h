#ifndef MAGIC_H
#define MAGIC_H


#include <string>
#include <vector>
#include <iostream>

using namespace std;
class Components;

class Magic {
    public:
        vector <Magic *> _listOfSpells;
        vector <Magic *> _listOfAllComponents;

        Magic();
        virtual ~Magic();

        void addNewSpell();
        void addNewComponent();

    protected:

    private:
};

#endif // MAGIC_H
