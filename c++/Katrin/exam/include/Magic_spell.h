#ifndef MAGIC_SPELL_H
#define MAGIC_SPELL_H

#include <Magic.h>

class Components;

class Magic_spell : public Magic {
    public:
        string title;
        vector <Components *> _listOfComponents;

        Magic_spell();
        Magic_spell(string title, vector <Components *> _listOfComponents);

        virtual ~Magic_spell();

        void addComponent();

    protected:

    private:
};

#endif // MAGIC_SPELL_H
