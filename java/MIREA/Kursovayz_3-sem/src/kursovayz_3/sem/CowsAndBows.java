package kursovayz_3.sem;
import java.awt.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class CowsAndBows {
    Console console = new Console();
    Scanner in = new Scanner(System.in);

    private static final int currentNumber = 0;
    private static final int _SIZE = 4;
    private static boolean _isGameEnd = false;

    private static final int randomX = 1 + (int) (Math.random() * 9); 
    private static final int randomY = 1 + (int) (Math.random() * 9);
    private static final int randomZ = 1 + (int) (Math.random() * 9);
    private static final int randomK = 1 + (int) (Math.random() * 9);

    private int[] mass = {
    	randomX, 
    	randomY, 
    	randomZ, 
    	randomK
    };

    // это для отладки)
    private int[] table = { 0,0,0,0 };

    private void showNumber () {
    	console.log(mass);
    }
 

    // это временно, тоже для отладки)
    public void startGame (javax.swing.JTable SecondGame_table) {
       
        boolean flag = true;
        
        while (flag) {
            flag = false;
        
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if ((i != j) && (mass[i] == mass[j])) {
                        flag = true;
                        break;
                    }
                }
            }
            
            if (flag) {
                for (int i = 0; i < 4; i++) {
                    mass[i] = 1 + (int) (Math.random() * 9);
                }
            } else {
                showNumber();
            }
        }
        
//        while ()

        for (int i = 0; i <= 20; i++) {
            SecondGame_table.setValueAt(i+1, i, 0);
        }
    }

    public void setNumber(javax.swing.JTable SecondGame_table, int number, int x, int y) {
        showNumber();
        // смещать по Х
        // сравнивать в таблице по У
        
        table[y-1] = number;
        
    	if (number == mass[y-1]) {
            console.log("Б"); //заменить на вывод в table[pos] res..
            SecondGame_table.setValueAt("Б", x, y+4);
    	} else {
            for (int i = 0; i < _SIZE; i++) {
                if (number == mass[i]) {
                    console.log("K"); //заменить на вывод в table[pos] res..
                    SecondGame_table.setValueAt("К", x, y+4);
                    break;
                }
            }
    	}

    	checkEnd();
    	if (_isGameEnd) {
            console.log("Конец игры!"); //вывести окошко с сообщением...
            JOptionPane.showMessageDialog(null, "Конец игры!!", "Вы молодец!", 1);
        }
    }

    private void checkEnd () {
    	boolean flag = true;

    	for (int i = 0; i < _SIZE; i++) {
            if (mass[i] != table[i]) {
                flag = false;
                break;
            }
    	}

    	_isGameEnd = flag;
    }
}
